export class AppSettings {
  public static API_URL: string = 'https://881be530.ngrok.io/graphql';
  public static MAP_URL: string = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='
  public static BOT_URL: string = 'https://8b248b49.ngrok.io/webhooks/fb';
}
